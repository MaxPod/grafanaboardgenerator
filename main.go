package main

import (
	"flag"
	"log"

	"boardgen/config"
	"boardgen/generator"
	"boardgen/grafana7"
	"boardgen/grafana8"
)

type BoardConstructor interface {
	GetBoardJSON(cfg *config.Config) *[]byte
}

func main() {
	configFile  := flag.String("f", "config.yml", "path to config.yml")
    flag.Parse()

	// config init
	cfg, err := config.InitConfig(*configFile)
	if err != nil {
		log.Fatalln(err)
	}

	// BoardConstructor
	var bc BoardConstructor

	switch cfg.Main.GrafanaVersion {
	case "7":
		bc = grafana7.NewBoard()
	case "8":
		bc = grafana8.NewBoard()	
	default:
		log.Fatalln("Unknown version Grafana")
	}

	data := bc.GetBoardJSON(cfg)

	// save json to file
	generator.ExportFile(cfg.Main.OutputFile, data)
}
