package grafana7

// Panel - rows
// nolint:tagliatelle // grafana json tags
type PanelRow struct {
	Collapsed  bool          `json:"collapsed"`
	Datasource interface{}   `json:"datasource"`
	Gridpos    Gridpos       `json:"gridPos"`
	ID         int           `json:"id"`
	Title      string        `json:"title"`
	Type       string        `json:"type"`
	Panels     []interface{} `json:"panels"`
}
