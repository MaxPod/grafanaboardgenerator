package grafana7

import (
	"boardgen/config"
)

// funcRow -  заполняет структуру строки для панели func_row
// nolint:gomnd // we use magic numbers
func (bc *BoardConstructor) funcRow(cfgPanel *config.Panel) *PanelRow {
	bc.item++

	p := PanelRow{
		Collapsed:  cfgPanel.Collapsed,
		Datasource: nil,
		Gridpos: Gridpos{
			H: 1,
			W: 24,
			X: 0,
			Y: bc.yCoordinate,
		},
		ID:     bc.item,
		Title:  cfgPanel.RowTitle,
		Type:   "row",
		Panels: []interface{}{},
	}

	return &p
}

// funcRowGraph  заполняет структуру графа  для панели func_row
// nolint:gomnd,funlen // we use magic numbers
func (bc *BoardConstructor) funcRowGraph(cfg *config.Panel) *PanelGraph {
	bc.item++

	p := PanelGraph{
		Aliascolors: struct{}{},
		Bars:        true,
		Dashlength:  10,
		Dashes:      false,
		Datasource:  cfg.DataSource,
		Fieldconfig: GraphFieldconfig{
			Defaults: GraphDefaults{
				Custom: GraphCustom{},
			},
			Overrides: []interface{}{},
		},
		Fill:         1,
		Fillgradient: 0,
		Gridpos: Gridpos{
			H: 10,
			W: 16,
			X: 0,
			Y: bc.yCoordinate,
		},
		Hiddenseries: false,
		ID:           bc.item,
		Legend: GraphLegend{
			Alignastable: true,
			Avg:          false,
			Current:      false,
			Max:          false,
			Min:          false,
			Rightside:    true,
			Show:         true,
			Total:        true,
			Values:       true,
		},
		Lines:         false,
		Linewidth:     0,
		Nullpointmode: "null as zero",
		Options: GraphOptions{
			Alertthreshold: true,
		},
		Percentage:    false,
		Pluginversion: "7.4.2",
		Pointradius:   0,
		Points:        false,
		Renderer:      "flot",
		Seriesoverrides: []GraphSeriesoverride{
			{
				Alias:       "/Response/",
				Legend:      false,
				Bars:        false,
				Lines:       true,
				Linewidth:   2,
				Fill:        0,
				Color:       "rgb(255, 255, 255)",
				Yaxis:       2,
				Points:      false,
				Pointradius: 3,
			},
		},
		Spacelength: 10,
		Stack:       true,
		Steppedline: false,
		Thresholds:  []interface{}{},
		Timefrom:    nil,
		Timeregions: []interface{}{},
		Timeshift:   nil,
		Title:       cfg.Graph.Title,
		Tooltip: GraphTooltip{
			Shared:    true,
			Sort:      0,
			ValueType: "individual",
		},
		Type: "graph",
		Xaxis: GraphXaxis{
			Buckets: nil,
			Mode:    "time",
			Name:    nil,
			Show:    true,
			Values:  []interface{}{},
		},
		Yaxes: []GraphYaxes{
			{
				Format:  "short",
				Label:   nil,
				Logbase: 1,
				Max:     nil,
				Min:     nil,
				Show:    true,
			},
			{
				Format:  "short",
				Label:   nil,
				Logbase: 1,
				Max:     nil,
				Min:     nil,
				Show:    true,
			},
		},
		Yaxis: GraphYaxis{
			Align:      false,
			Alignlevel: nil,
		},
	}

	targets := []Target{
		{
			Alias: "$tag_" + cfg.Tag,
			Groupby: []TargetItem{
				{
					Params: []string{
						cfg.Graph.BarInterval,
					},
					Type: "time",
				},
				{
					Params: []string{
						cfg.Tag,
					},
					Type: "tag",
				},
			},
			Measurement:  cfg.Metric,
			Orderbytime:  "ASC",
			Policy:       "default",
			Refid:        "A",
			Resultformat: "time_series",
			Select: [][]TargetItem{
				{
					{
						Params: []string{
							"duration",
						},
						Type: "field",
					},
					{
						Params: []string{},
						Type:   "count",
					},
				},
			},
		},
		{
			Alias: "Response Time:",
			Groupby: []TargetItem{
				{
					Params: []string{
						cfg.Graph.BarInterval,
					},
					Type: "time",
				},
			},
			Measurement:  cfg.Metric,
			Orderbytime:  "ASC",
			Policy:       "default",
			Refid:        "B",
			Resultformat: "time_series",
			Select: [][]TargetItem{
				{
					{
						Params: []string{
							"duration",
						},
						Type: "field",
					},
					{
						Params: []string{},
						Type:   "max",
					},
				},
			},
		},
	}

	p.Targets = targets

	return &p
}

// funcRowTotal -  заполняет структуру Total для панели func_row
// nolint:gomnd // bad linter, we use magic
func (bc *BoardConstructor) funcRowTotal(cfgPanel *config.Panel) *PanelStat {
	bc.item++

	p := PanelStat{
		Datasource: cfgPanel.DataSource,
		Fieldconfig: Fieldconfig{
			Defaults: StatDefaults{
				Color: StatColor{
					Mode: "thresholds",
				},
				Thresholds: StatThresholds{
					Mode: "absolute",
					Steps: []StatThresholdsStep{
						{
							Color: "green",
							Value: nil,
						},
					},
				},
				NoValue: "0",
			},
		},
		Gridpos: Gridpos{
			H: 3,
			W: 4,
			X: 16,
			Y: bc.yCoordinate,
		},
		ID: bc.item,
		Options: StatOptions{
			Colormode:   "value",
			Graphmode:   "none",
			Justifymode: "auto",
			Orientation: "auto",
			Reduceoptions: Reduceoptions{
				Calcs: []string{
					"lastNotNull",
				},
			},
			Textmode: "auto",
		},
		Pluginversion: "7.5.12",
		Targets: []Target{
			{
				Groupby:      []TargetItem{},
				Measurement:  cfgPanel.Metric,
				Orderbytime:  "ASC",
				Policy:       "default",
				Refid:        "A",
				Resultformat: "time_series",
				Select: [][]TargetItem{
					{
						{
							Params: []string{
								cfgPanel.Graph.Duration,
							},
							Type: "field",
						},
						{
							Params: []string{},
							Type:   "count",
						},
					},
				},
			},
		},
		Title: cfgPanel.Total.Title,
		Type:  "stat",
	}

	return &p
}

// funcRowTotal -  заполняет структуру Total для панели func_row
// nolint:gomnd // bad linter, we use magic
func (bc *BoardConstructor) funcRowErrors(cfgPanel *config.Panel) *PanelStat {
	bc.item++

	p := PanelStat{
		Datasource: cfgPanel.DataSource,
		Fieldconfig: Fieldconfig{
			Defaults: StatDefaults{
				Color: StatColor{
					Mode: "thresholds",
				},
				Thresholds: StatThresholds{
					Mode: "absolute",
					Steps: []StatThresholdsStep{
						{
							Color: "white",
							Value: nil,
						},
					},
				},
				NoValue: "0",
			},
		},
		Gridpos: Gridpos{
			H: 3,
			W: 4,
			X: 20,
			Y: bc.yCoordinate,
		},
		ID: bc.item,
		Options: StatOptions{
			Colormode:   "value",
			Graphmode:   "none",
			Justifymode: "auto",
			Orientation: "auto",
			Reduceoptions: Reduceoptions{
				Calcs: []string{
					"lastNotNull",
				},
			},
			Textmode: "auto",
		},
		Pluginversion: "7.5.12",
		Targets: []Target{
			{
				Groupby:      []TargetItem{},
				Measurement:  cfgPanel.Metric,
				Orderbytime:  "ASC",
				Policy:       "default",
				Refid:        "A",
				Resultformat: "time_series",
				Select: [][]TargetItem{
					{
						{
							Params: []string{
								cfgPanel.Graph.Duration,
							},
							Type: "field",
						},
						{
							Params: []string{},
							Type:   "count",
						},
					},
				},
				Tags: []TargetTag{
					{
						Key:      cfgPanel.Tag,
						Operator: "=",
						Value:    "error",
					},
				},
			},
		},
		Title: cfgPanel.Errors.Title,
		Type:  "stat",
	}

	return &p
}

// funcRowDonut -  заполняет структуру Result c типом donut для панели func_row
// nolint:gomnd // we use magic
func (bc *BoardConstructor) funcRowDonut(cfgPanel *config.Panel) *PanelDonut {
	bc.item++

	p := PanelDonut{
		Breakpoint: "50%",
		Combine: Combine{
			Label:     "Others",
			Threshold: 0,
		},
		Datasource: cfgPanel.DataSource,
		Fontsize:   "80%",
		Format:     "short",
		Gridpos: Gridpos{
			H: 7,
			W: 8,
			X: 16,
			Y: bc.yCoordinate + 4,
		},
		ID: bc.item,
		Legend: DonutLegend{
			Percentage: true,
			Show:       true,
			Values:     true,
		},
		Legendtype:    "Right side",
		Nullpointmode: "connected",
		Pietype:       "donut",
		Pluginversion: "7.5.12",
		Strokewidth:   1,
		Targets: []Target{
			{
				Alias: "$tag_" + cfgPanel.Tag,
				Groupby: []TargetItem{
					{
						Params: []string{
							cfgPanel.Tag,
						},
						Type: "tag",
					},
				},
				Measurement:  cfgPanel.Metric,
				Orderbytime:  "ASC",
				Policy:       "default",
				Refid:        "A",
				Resultformat: "time_series",
				Select: [][]TargetItem{
					{
						{
							Params: []string{
								"duration",
							},
							Type: "field",
						},
						{
							Params: []string{},
							Type:   "count",
						},
					},
				},
			},
		},
		Title:     cfgPanel.Result.Title,
		Type:      "grafana-piechart-panel",
		Valuename: "current",
	}

	return &p
}

// funcRowHistogram - fill Histogram panel
// nolint:gomnd // we use magic
func (bc *BoardConstructor) funcRowHistogram(cfgPanel *config.Panel) *PanelGraph {
	bc.item++

	p := PanelGraph{
		Aliascolors: struct{}{},
		Bars:        true,
		Dashlength:  10,
		Dashes:      false,
		Datasource:  cfgPanel.DataSource,
		Fieldconfig: GraphFieldconfig{
			Defaults: GraphDefaults{
				Custom: GraphCustom{},
			},
			Overrides: []interface{}{},
		},
		Fill:         1,
		Fillgradient: 0,
		Gridpos: Gridpos{
			H: 7,
			W: 8,
			X: 16,
			Y: bc.yCoordinate + 6,
		},
		Hiddenseries: false,
		ID:           bc.item,
		Legend: GraphLegend{
			Alignastable: false,
			Avg:          true,
			Current:      false,
			Max:          true,
			Min:          true,
			Rightside:    false,
			Show:         true,
			Total:        false,
			Values:       true,
		},
		Lines:         false,
		Linewidth:     0,
		Nullpointmode: "null as zero",
		Options: GraphOptions{
			Alertthreshold: true,
		},
		Percentage:      false,
		Pluginversion:   "7.4.2",
		Pointradius:     0,
		Points:          false,
		Renderer:        "flot",
		Seriesoverrides: []GraphSeriesoverride{},
		Spacelength:     10,
		Stack:           true,
		Steppedline:     false,
		Thresholds:      []interface{}{},
		Timefrom:        nil,
		Timeregions:     []interface{}{},
		Timeshift:       nil,
		Title:           cfgPanel.Result.Title,
		Tooltip: GraphTooltip{
			Shared:    true,
			Sort:      0,
			ValueType: "individual",
		},
		Type: "graph",
		Xaxis: GraphXaxis{
			Buckets: nil,
			Mode:    "histogram",
			Name:    nil,
			Show:    true,
			Values:  []interface{}{},
		},
		Yaxes: []GraphYaxes{
			{
				Format:  "short",
				Label:   nil,
				Logbase: 1,
				Max:     nil,
				Min:     nil,
				Show:    true,
			},
			{
				Format:  "short",
				Label:   nil,
				Logbase: 1,
				Max:     nil,
				Min:     nil,
				Show:    true,
			},
		},
		Yaxis: GraphYaxis{
			Align:      false,
			Alignlevel: nil,
		},
	}

	targets := []Target{
		{
			Alias:        cfgPanel.Result.Fields[0],
			Groupby:      []TargetItem{},
			Measurement:  cfgPanel.Metric,
			Orderbytime:  "ASC",
			Policy:       "default",
			Refid:        "B",
			Resultformat: "time_series",
			Select: [][]TargetItem{
				{
					{
						Params: []string{
							cfgPanel.Result.Fields[0],
						},
						Type: "field",
					},
				},
			},
			Tags: []TargetTag{
				{
					Key:      cfgPanel.Tag,
					Operator: "!=",
					Value:    "error",
				},
			},
		},
	}

	p.Targets = targets

	return &p
}
