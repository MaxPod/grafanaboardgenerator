package grafana7

// Panel Donut
// nolint:tagliatelle // grafana json tags
type PanelDonut struct {
	Aliascolors  struct{}    `json:"aliasColors"`
	Breakpoint   string      `json:"breakPoint"`
	Cachetimeout interface{} `json:"cacheTimeout"`
	Combine      Combine     `json:"combine"`
	Datasource   string      `json:"datasource"`
	Fieldconfig  struct {
		Defaults  struct{}      `json:"defaults"`
		Overrides []interface{} `json:"overrides"`
	} `json:"fieldConfig"`
	Fontsize        string        `json:"fontSize"`
	Format          string        `json:"format"`
	Gridpos         Gridpos       `json:"gridPos"`
	ID              int           `json:"id"`
	Interval        interface{}   `json:"interval"`
	Legend          DonutLegend   `json:"legend"`
	Legendtype      string        `json:"legendType"`
	Links           []interface{} `json:"links"`
	Nullpointmode   string        `json:"nullPointMode"`
	Pietype         string        `json:"pieType"`
	Pluginversion   string        `json:"pluginVersion"`
	Strokewidth     int           `json:"strokeWidth"`
	Targets         []Target      `json:"targets"`
	Timefrom        interface{}   `json:"timeFrom"`
	Timeshift       interface{}   `json:"timeShift"`
	Title           string        `json:"title"`
	Transformations []interface{} `json:"transformations"`
	Type            string        `json:"type"`
	Valuename       string        `json:"valueName"`
}

// Donut subtype
type Combine struct {
	Label     string `json:"label"`
	Threshold int    `json:"threshold"`
}

type DonutLegend struct {
	Percentage bool `json:"percentage"`
	Show       bool `json:"show"`
	Values     bool `json:"values"`
}
