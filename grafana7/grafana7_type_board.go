package grafana7

// Board types
// nolint:tagliatelle // grafana json tags
type Board struct {
	Annotations   Annotations   `json:"annotations"`
	Editable      bool          `json:"editable"`
	Gnetid        interface{}   `json:"gnetId"`
	Graphtooltip  int           `json:"graphTooltip"`
	ID            int           `json:"id"`
	Links         []interface{} `json:"links"`
	Panels        []interface{} `json:"panels"`
	Schemaversion int           `json:"schemaVersion"`
	Style         string        `json:"style"`
	Tags          []interface{} `json:"tags"`
	Templating    Templating    `json:"templating"`
	Time          Time          `json:"time"`
	Timepicker    Timepicker    `json:"timepicker"`
	Timezone      string        `json:"timezone"`
	Title         string        `json:"title"`
	UID           string        `json:"uid"`
	Version       int           `json:"version"`
}

// Board subtype
type Annotations struct {
	List []List `json:"list"`
}

// Annotations subtype
// nolint:tagliatelle // grafana json tags
type List struct {
	Builtin    int    `json:"builtIn"`
	Datasource string `json:"datasource"`
	Enable     bool   `json:"enable"`
	Hide       bool   `json:"hide"`
	Iconcolor  string `json:"iconColor"`
	Name       string `json:"name"`
	Type       string `json:"type"`
}

// Board subtype
type Templating struct {
	List []Variable `json:"list"`
}

// Templating subtype
// nolint:tagliatelle // grafana json tags
type Variable struct {
	Allvalue    interface{} `json:"allValue"`
	Current     Current     `json:"current"`
	Description string      `json:"description"`
	Error       interface{} `json:"error"`
	Hide        int         `json:"hide"`
	Includeall  bool        `json:"includeAll"`
	Label       string      `json:"label"`
	Multi       bool        `json:"multi"`
	Name        string      `json:"name"`
	Options     []Options   `json:"options"`
	Query       string      `json:"query"`
	QueryValue  string      `json:"queryValue"`
	Skipurlsync bool        `json:"skipUrlSync"`
	Type        string      `json:"type"`
}

// Variable subtype
type Current struct {
	Selected bool   `json:"selected"`
	Text     string `json:"text"`
	Value    string `json:"value"`
}

// Variable subtype
type Options struct {
	Selected bool   `json:"selected"`
	Text     string `json:"text"`
	Value    string `json:"value"`
}

// Board subtype
type Time struct {
	From string `json:"from"`
	To   string `json:"to"`
}

// Board subtype
type Timepicker struct{}
