package generator

import (
	"fmt"
	"os"
)

func ExportFile(path string, data *[]byte) {
	// open output file
	fo, err := os.Create(path)
	if err != nil {
		fmt.Println(err)
	}

	// close fo on exit and check for its returned error
	defer func() {
		if err := fo.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	_, err = fo.Write(*data)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Create grafana board in", path)
}
