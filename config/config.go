package config

// Config struct
type Config struct {
	Main      Main       `yaml:"main"`
	Board     Board      `yaml:"board"`
	Panels    []Panel    `yaml:"panels"`
	Variables []Variable `yaml:"variables"`
}

type Main struct {
	GrafanaVersion string `yaml:"grafana_version"`
	OutputFile     string `yaml:"output_file"`
}

type Board struct {
	Title string `yaml:"title"`
	UID   string `yaml:"uid"`
}

// type FuncAsDonut struct {
// 	RowTitle    string `yaml:"row_title"`
// 	Metric      string `yaml:"metric"`
// 	Duration    string `yaml:"duration"`
// 	BarInterval int    `yaml:"bar_interval"`
// 	LegendTag   string `yaml:"legend_tag"`
// 	GraphTitle  string `yaml:"graph_title"`
// 	TotalTitle  string `yaml:"total_title"`
// 	ErrorTitle  string `yaml:"error_title"`
// 	DonatTitle  string `yaml:"donat_title"`
// }

type Graph struct {
	Title       string `yaml:"title"`
	Duration    string `yaml:"duration"`
	BarInterval string `yaml:"bar_interval"`
}

type Total struct {
	Title string `yaml:"title"`
}

type Errors struct {
	Title string `yaml:"title"`
}

type Result struct {
	Type   string   `yaml:"type"`
	Title  string   `yaml:"title"`
	Fields []string `yaml:"fields"`
}

type FuncRow struct {
	RowTitle  string `yaml:"row_title"`
	Metric    string `yaml:"metric"`
	LegendTag string `yaml:"legend_tag"`
	Graph     Graph  `yaml:"graph"`
	Total     Total  `yaml:"total"`
	Errors    Errors `yaml:"errors"`
	Result    Result `yaml:"result"`
}

type Panel struct {
	Type  string `yaml:"type"`
	Title string `yaml:"title"`
	// row
	Collapsed bool `yaml:"collapsed"`

	// graph
	Targets []Target `yaml:"targets"`

	// func_row
	RowTitle   string `yaml:"row_title"`
	DataSource string `yaml:"datasource"`
	Metric     string `yaml:"metric"`
	Tag        string `yaml:"tag"`
	Graph      Graph  `yaml:"graph"`
	Total      Total  `yaml:"total"`
	Errors     Errors `yaml:"errors"`
	Result     Result `yaml:"result"`

	// counters
	Counters []Counter `yaml:"counters"`
}

type Target struct {
	Refid       string `yaml:"refid"`
	Alias       string `yaml:"alias"`
	GroupBy     []Item `yaml:"group_by"`
	Measurement string `yaml:"measurement"`
	Query       string `yaml:"query"`
	Select      []Item `yaml:"select"`
}

type Item struct {
	Params string `yaml:"params"`
	Type   string `yaml:"type"`
}

type Variable struct {
	Name        string   `yaml:"name"`
	Label       string   `yaml:"label"`
	Description string   `yaml:"description"`
	Query       string   `yaml:"query"`
	Options     []Option `yaml:"options"`
}

type Option struct {
	Selected bool   `yaml:"selected"`
	Text     string `yaml:"text"`
	Value    string `yaml:"value"`
}

type Counter struct {
	Title      string       `yaml:"title"`
	Width      int          `yaml:"width"`
	DataSource string       `yaml:"datasource"`
	Metric     string       `yaml:"metric"`
	Field      string       `yaml:"field"`
	Tags       CounterTag `yaml:"tags"`
}

type CounterTag struct {
	Key      string `yaml:"key"`
	Operator string `yaml:"operator"`
	Value    string `yaml:"value"`
}
