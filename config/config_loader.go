package config

import (
	"fmt"

	"github.com/jinzhu/configor"
)

// InitConfig load config params from config.yml
// nolint:forbidigo // we use fmt
func InitConfig(filePath string) (*Config, error) {
	config := &Config{}

	// load config params from config.yml, missed params configor try load from env
	err := configor.Load(config, filePath)
	if err != nil {
		return nil, fmt.Errorf("configor.Load(file=%s):%w", "config.yml", err)
	}

	fmt.Println("Load config from file", filePath)

	return config, nil
}
