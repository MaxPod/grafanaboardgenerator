package grafana8

// Panel Graph
// nolint:tagliatelle // grafana json tags
type PanelGraph struct {
	Aliascolors     struct{}              `json:"aliasColors"`
	Bars            bool                  `json:"bars"`
	Dashlength      int                   `json:"dashLength"`
	Dashes          bool                  `json:"dashes"`
	Datasource      string                `json:"datasource"`
	Fieldconfig     GraphFieldconfig      `json:"fieldConfig"`
	Fill            int                   `json:"fill"`
	Fillgradient    int                   `json:"fillGradient"`
	Gridpos         Gridpos               `json:"gridPos"`
	Hiddenseries    bool                  `json:"hiddenSeries"`
	ID              int                   `json:"id"`
	Legend          GraphLegend           `json:"legend"`
	Lines           bool                  `json:"lines"`
	Linewidth       int                   `json:"linewidth"`
	Nullpointmode   string                `json:"nullPointMode"`
	Options         GraphOptions          `json:"options"`
	Percentage      bool                  `json:"percentage"`
	Pluginversion   string                `json:"pluginVersion"`
	Pointradius     int                   `json:"pointradius"`
	Points          bool                  `json:"points"`
	Renderer        string                `json:"renderer"`
	Seriesoverrides []GraphSeriesoverride `json:"seriesOverrides"`
	Spacelength     int                   `json:"spaceLength"`
	Stack           bool                  `json:"stack"`
	Steppedline     bool                  `json:"steppedLine"`
	Targets         []Target              `json:"targets"`
	Thresholds      []interface{}         `json:"thresholds"`
	Timefrom        interface{}           `json:"timeFrom"`
	Timeregions     []interface{}         `json:"timeRegions"`
	Timeshift       interface{}           `json:"timeShift"`
	Title           string                `json:"title"`
	Tooltip         GraphTooltip          `json:"tooltip"`
	Type            string                `json:"type"`
	Xaxis           GraphXaxis            `json:"xaxis"`
	Yaxes           []GraphYaxes          `json:"yaxes"`
	Yaxis           GraphYaxis            `json:"yaxis"`
}

type GraphFieldconfig struct {
	Defaults  GraphDefaults `json:"defaults"`
	Overrides []interface{} `json:"overrides"`
}

type GraphDefaults struct {
	Custom GraphCustom `json:"custom"`
}

type GraphCustom struct{}

// nolint:tagliatelle // grafana json tags
type GraphLegend struct {
	Alignastable bool `json:"alignAsTable"`
	Avg          bool `json:"avg"`
	Current      bool `json:"current"`
	Max          bool `json:"max"`
	Min          bool `json:"min"`
	Rightside    bool `json:"rightSide"`
	Show         bool `json:"show"`
	Total        bool `json:"total"`
	Values       bool `json:"values"`
}

// nolint:tagliatelle // grafana json tags
type GraphOptions struct {
	Alertthreshold bool `json:"alertThreshold"`
}

type GraphSeriesoverride struct {
	Alias       string `json:"alias"`
	Legend      bool   `json:"legend"`
	Bars        bool   `json:"bars"`
	Lines       bool   `json:"lines"`
	Linewidth   int    `json:"linewidth"`
	Color       string `json:"color"`
	Yaxis       int    `json:"yaxis"`
	Points      bool   `json:"points"`
	Pointradius int    `json:"pointradius"`
	Fill        int    `json:"fill"`
}

// nolint:tagliatelle // grafana json tags
type GraphTarget struct {
	Alias        string              `json:"alias"`
	Groupby      []GraphTargetItem   `json:"groupBy"`
	Measurement  string              `json:"measurement"`
	Orderbytime  string              `json:"orderByTime"`
	Policy       string              `json:"policy"`
	Query        string              `json:"query"`
	Rawquery     bool                `json:"rawQuery"`
	Refid        string              `json:"refId"`
	Resultformat string              `json:"resultFormat"`
	Select       [][]GraphTargetItem `json:"select"`
	Tags         []interface{}       `json:"tags"`
}

type GraphTargetItem struct {
	Params []string `json:"params"`
	Type   string   `json:"type"`
}

type GraphTooltip struct {
	Shared    bool   `json:"shared"`
	Sort      int    `json:"sort"`
	ValueType string `json:"value_type"`
}

type GraphXaxis struct {
	Buckets interface{}   `json:"buckets"`
	Mode    string        `json:"mode"`
	Name    interface{}   `json:"name"`
	Show    bool          `json:"show"`
	Values  []interface{} `json:"values"`
}

// nolint:tagliatelle // grafana json tags
type GraphYaxes struct {
	Format  string      `json:"format"`
	Label   interface{} `json:"label"`
	Logbase int         `json:"logBase"`
	Max     interface{} `json:"max"`
	Min     interface{} `json:"min"`
	Show    bool        `json:"show"`
}

// nolint:tagliatelle // grafana json tags
type GraphYaxis struct {
	Align      bool        `json:"align"`
	Alignlevel interface{} `json:"alignLevel"`
}
