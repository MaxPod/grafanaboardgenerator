package grafana8

// Global Grafana Types

// Gridpos -  panel position struct
type Gridpos struct {
	H int `json:"h"`
	W int `json:"w"`
	X int `json:"x"`
	Y int `json:"y"`
}

type TargetTag struct {
	Key      string `json:"key"`
	Operator string `json:"operator"`
	Value    string `json:"value"`
}

// nolint:tagliatelle // grafana json tags
type Target struct {
	Alias        string         `json:"alias"`
	Groupby      []TargetItem   `json:"groupBy"`
	Measurement  string         `json:"measurement"`
	Orderbytime  string         `json:"orderByTime"`
	Policy       string         `json:"policy"`
	Query        string         `json:"query"`
	Rawquery     bool           `json:"rawQuery"`
	Refid        string         `json:"refId"`
	Resultformat string         `json:"resultFormat"`
	Select       [][]TargetItem `json:"select"`
	Tags         []TargetTag    `json:"tags"`
}

type TargetItem struct {
	Params []string `json:"params"`
	Type   string   `json:"type"`
}
