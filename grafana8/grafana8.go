package grafana8

// nolint:gofumpt // crazy linter
import (
	"encoding/json"
	"fmt"

	"boardgen/config"
)

type BoardConstructor struct {
	item        int
	yCoordinate int
}

func NewBoard() *BoardConstructor {
	return &BoardConstructor{
		item:        0,
		yCoordinate: 0,
	}
}

// nolint:gomnd,forbidigo // we use magic numbers
func (bc *BoardConstructor) GetBoardJSON(cfg *config.Config) *[]byte {
	board := Board{
		Annotations: Annotations{
			List: []List{
				{
					Builtin:    1,
					Datasource: "-- Grafana --",
					Enable:     true,
					Hide:       true,
					Iconcolor:  "rgba(0, 211, 255, 1)",
					Name:       "Annotations & Alerts",
					Type:       "dashboard",
				},
			},
		},
		Editable:      true,
		Gnetid:        nil,
		Graphtooltip:  0,
		ID:            2,
		Links:         []interface{}{},
		Schemaversion: 27,
		Style:         "dark",
		Tags:          []interface{}{},
		Time: Time{
			From: "now-6h",
			To:   "now",
		},
		Timepicker: Timepicker{},
		Timezone:   "",
		Title:      cfg.Board.Title,
		UID:        cfg.Board.UID,
		Version:    1,
	}

	// вывод панелей
	for i := range cfg.Panels {
		switch cfg.Panels[i].Type {
		case "header":
			panelRow := bc.funcRow(&cfg.Panels[i])
			bc.yCoordinate++

			panelText := bc.fillPanelText(&cfg.Panels[i])
			bc.yCoordinate++

			board.Panels = append(board.Panels, *panelRow, *panelText)

		case "func_row":
			panelRow := bc.funcRow(&cfg.Panels[i])
			bc.yCoordinate++

			panelGraph := bc.funcRowGraph(&cfg.Panels[i])

			panelTotal := bc.funcRowTotal(&cfg.Panels[i])

			panelErrors := bc.funcRowErrors(&cfg.Panels[i])

			var panelResult interface{}

			switch cfg.Panels[i].Result.Type {
			case "donut":
				res := bc.funcRowDonut(&cfg.Panels[i])
				panelResult = *res
			case "histogram":
				res := bc.funcRowHistogram(&cfg.Panels[i])
				panelResult = *res
			}

			if cfg.Panels[i].Collapsed {
				//  для свернутых строк добавляем панели в массив строки
				panelRow.Panels = append(panelRow.Panels, *panelGraph, *panelTotal, *panelErrors, panelResult)
				board.Panels = append(board.Panels, *panelRow)
			} else {
				// для развернутых строк добавляем панели в массив борды
				board.Panels = append(board.Panels, *panelRow, *panelGraph, *panelTotal, *panelErrors, panelResult)
			}

			bc.yCoordinate += 10

		case "counters":
			panelRow := bc.funcRow(&cfg.Panels[i])
			bc.yCoordinate++

			var panelsCounter []PanelStat

			for countersIdx := range cfg.Panels[i].Counters {
				panelsCounter = append(panelsCounter, *bc.panelCounter(&cfg.Panels[i], countersIdx))
			}

			bc.yCoordinate += 3

			if cfg.Panels[i].Collapsed {
				//  для свернутых строк добавляем панели в массив строки
				for countersIdx := range panelsCounter {
					panelRow.Panels = append(panelRow.Panels, panelsCounter[countersIdx])
				}

				panelRow.Panels = append(panelRow.Panels, panelsCounter)
				board.Panels = append(board.Panels, *panelRow)
			} else {
				// для развернутых строк добавляем панели в массив борды
				board.Panels = append(board.Panels, *panelRow)
				for panelsCounterIdx := range panelsCounter {
					board.Panels = append(board.Panels, panelsCounter[panelsCounterIdx])
				}
			}
		}
	}

	boardJSON, err := json.Marshal(board)
	if err != nil {
		fmt.Println(err)
	}

	return &boardJSON
}

// addVariables - заполнение переменных в Templates
// func addVariables(cfg *config.Config, board *Board) {
// 	// вывод переменных
// 	templating := Templating{}
// 	for i := range cfg.Variables {
// 		boardVariable := Variable{
// 			Allvalue: nil,
// 			// Current: Current{
// 			// 	Selected: false,
// 			// 	Text:     "",
// 			// 	Value:    "",
// 			// },
// 			Description: cfg.Variables[i].Description,
// 			Error:       nil,
// 			Hide:        0,
// 			Includeall:  false,
// 			Label:       cfg.Variables[i].Label,
// 			Multi:       false,
// 			Name:        cfg.Variables[i].Name,
// 			//Options:     []Options{},
// 			Query:       cfg.Variables[i].Query,
// 			QueryValue:  "",
// 			Skipurlsync: false,
// 			Type:        "custom",
// 		}

// 		for j := range cfg.Variables[i].Options {
// 			options := Options{
// 				Selected: cfg.Variables[i].Options[j].Selected,
// 				Text:     cfg.Variables[i].Options[j].Text,
// 				Value:    cfg.Variables[i].Options[j].Value,
// 			}

// 			boardVariable.Options = append(boardVariable.Options, options)

// 			// current
// 			if options.Selected {
// 				current := Current{
// 					Selected: false,
// 					Text:     options.Text,
// 					Value:    options.Value,
// 				}

// 				boardVariable.Current = current
// 			}
// 		}
// 		templating.List = append(templating.List, boardVariable)
// 	}
// 	board.Templating = templating
// }
