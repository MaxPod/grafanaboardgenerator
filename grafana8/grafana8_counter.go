package grafana8

import (
	"boardgen/config"
)

// panelCounter -  заполняет структуру Counter для панели Counters
// nolint:gomnd // bad linter, we use magic
func (bc *BoardConstructor) panelCounter(cfgPanel *config.Panel, counterIndex int) *PanelStat {
	bc.item++

	p := PanelStat{
		Datasource: cfgPanel.Counters[counterIndex].DataSource,
		Fieldconfig: Fieldconfig{
			Defaults: StatDefaults{
				Color: StatColor{
					Mode: "thresholds",
				},
				Thresholds: StatThresholds{
					Mode: "absolute",
					Steps: []StatThresholdsStep{
						{
							Color: "green",
							Value: nil,
						},
					},
				},
				NoValue: "0",
			},
		},
		Gridpos: Gridpos{
			H: 3,
			W: cfgPanel.Counters[counterIndex].Width,
			X: bc.getXCoord(cfgPanel, counterIndex),
			Y: bc.yCoordinate,
		},
		ID: bc.item,
		Options: StatOptions{
			Colormode:   "value",
			Graphmode:   "none",
			Justifymode: "auto",
			Orientation: "auto",
			Reduceoptions: Reduceoptions{
				Calcs: []string{
					"lastNotNull",
				},
			},
			Textmode: "auto",
		},
		Pluginversion: "7.5.12",
		Targets: []Target{
			{
				Groupby:      []TargetItem{},
				Measurement:  cfgPanel.Counters[counterIndex].Metric,
				Orderbytime:  "ASC",
				Policy:       "default",
				Refid:        "A",
				Resultformat: "time_series",
				Select: [][]TargetItem{
					{
						{
							Params: []string{
								cfgPanel.Counters[counterIndex].Field,
							},
							Type: "field",
						},
					},
				},
				Tags: []TargetTag{
					{
						Key:      cfgPanel.Counters[counterIndex].Tags.Key,
						Operator: cfgPanel.Counters[counterIndex].Tags.Operator,
						Value:    cfgPanel.Counters[counterIndex].Tags.Value,
					},
				},
			},
		},
		Title: cfgPanel.Counters[counterIndex].Title,
		Type:  "stat",
	}

	return &p
}

// getXCoord возвращает координату текущей панели с учетом ширин предыдущих
func (bc *BoardConstructor) getXCoord(cfgPanel *config.Panel, counterIndex int) int {
	x := 0
	for i := 0; i < counterIndex; i++ {
		x += cfgPanel.Counters[i].Width
	}

	return x
}
