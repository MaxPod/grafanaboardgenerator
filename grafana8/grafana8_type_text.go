package grafana8

// Panel Text
// nolint:tagliatelle // grafana json tags
type PanelText struct {
	Datasource      interface{} `json:"datasource"`
	Content         string      `json:"content"`
	Gridpos         Gridpos     `json:"gridPos"`
	ID              int         `json:"id"`
	Maxperrow       int         `json:"maxPerRow"`
	Mode            string      `json:"mode"`
	Repeat          interface{} `json:"repeat"`
	Repeatdirection string      `json:"repeatDirection"`
	Timefrom        interface{} `json:"timeFrom"`
	Timeshift       interface{} `json:"timeShift"`
	Title           string      `json:"title"`
	Type            string      `json:"type"`
}
