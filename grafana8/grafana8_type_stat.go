package grafana8

// Panel Stat
// nolint:tagliatelle // grafana json tags
type PanelStat struct {
	Datasource    string      `json:"datasource"`
	Fieldconfig   Fieldconfig `json:"fieldConfig"`
	Gridpos       Gridpos     `json:"gridPos"`
	ID            int         `json:"id"`
	Options       StatOptions `json:"options"`
	Pluginversion string      `json:"pluginVersion"`
	Targets       []Target    `json:"targets"`
	Title         string      `json:"title"`
	Type          string      `json:"type"`
}

// PanelStat subtype
type Fieldconfig struct {
	Defaults  StatDefaults  `json:"defaults"`
	Overrides []interface{} `json:"overrides"`
}

// Fieldconfig subtype
// nolint:tagliatelle // Camel Case in Grafana
type StatDefaults struct {
	Color      StatColor      `json:"color"`
	Mappings   []interface{}  `json:"mappings"`
	Thresholds StatThresholds `json:"thresholds"`
	NoValue    string         `json:"noValue"`
}

// StatDefaults subtype
type StatColor struct {
	Mode string `json:"mode"`
}

// StatDefaults subtype
type StatThresholds struct {
	Mode  string               `json:"mode"`
	Steps []StatThresholdsStep `json:"steps"`
}

// StatThresholds subtype
type StatThresholdsStep struct {
	Color string      `json:"color"`
	Value interface{} `json:"value"`
}

// PanelStat subtype
// nolint:tagliatelle // grafana json tags
type StatOptions struct {
	Colormode     string        `json:"colorMode"`
	Graphmode     string        `json:"graphMode"`
	Justifymode   string        `json:"justifyMode"`
	Orientation   string        `json:"orientation"`
	Reduceoptions Reduceoptions `json:"reduceOptions"`
	Text          struct{}      `json:"text"`
	Textmode      string        `json:"textMode"`
}

// StatOptions subtype
type Reduceoptions struct {
	Calcs  []string `json:"calcs"`
	Fields string   `json:"fields"`
	Values bool     `json:"values"`
}
