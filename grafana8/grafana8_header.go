package grafana8

import (
	"boardgen/config"
)

// fillPanelText -  заполняет панель заголовка
// nolint:gomnd // we use magic
func (bc *BoardConstructor) fillPanelText(cfgPanel *config.Panel) *PanelText {
	bc.item++

	p := PanelText{
		Content: `<h1 style="text-align:center"> ` + cfgPanel.Title + ` </h1>`,
		Gridpos: Gridpos{
			H: 2,
			W: 24,
			X: 0,
			Y: bc.yCoordinate,
		},
		ID:              bc.item,
		Maxperrow:       2,
		Mode:            "html",
		Repeatdirection: "h",
		Type:            "text",
	}

	return &p
}
